FROM tomcat:8.0-jre8
MAINTAINER KrishnaChaitanya kctechnologies@gmail
COPY target/maven-java-project-1.2.jar  /usr/local/tomcat/webapps/maven-web-project-1.2.jar
EXPOSE 8083
CMD ["catalina.sh", "run"]
